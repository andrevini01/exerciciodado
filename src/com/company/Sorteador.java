package com.company;

import java.util.Random;

public class Sorteador {
    Dado dado = new Dado();
    InputOutput inputOutput = new InputOutput();
    Random random = new Random();

    public void sortear() {
        dado.setQuantidadeFaces(6);
        int numeroSorteado = random.nextInt(dado.getQuantidadeFaces())+1;
        inputOutput.imprimir(numeroSorteado);
    }
}
